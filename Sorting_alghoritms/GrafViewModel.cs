﻿using OxyPlot;
using OxyPlot.Legends;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Sorting_alghoritms
{
    public class GrafViewModel
    {
        public PlotModel GrafModel { get; private set; }
        public IList<DataPoint> DataPoints { get; set; }

        private Dictionary<string, LineSeries> series = new Dictionary<string, LineSeries>();

        private BubbleSort bubble = new BubbleSort(null);
        private CountingSort counting = new CountingSort(null);
        private HeapSort heap = new HeapSort(null);

        public GrafViewModel()
        {
            this.GrafModel = new PlotModel { Title = "Grafy" };
            this.GrafModel.Legends.Add(new Legend()
            {
                LegendTitle = "Legenda",
                LegendPosition = LegendPosition.TopRight
            });
            //Task.WaitAll(AddSerie(bubble, -10000, 10000), AddSerie(counting, -10000, 10000), AddSerie(heap, -10000, 10000));
            foreach (string key in series.Keys)
            {
                this.GrafModel.Series.Add(series[key]);
                
            }
            //MessageBox.Show("Hotovo");

        }

        /*** Nefunkční  ***/
        //private async Task AddSerie(ISortable sortalg, int min, int max)
        //{
        //    Stopwatch stopwatch = new Stopwatch();
        //    LineSeries serie = new LineSeries
        //    {
        //        Title = sortalg.Name,
        //        Color = sortalg.Color
        //    };
            
        //    for (int i = 10; i <= 1000; i += 10)
        //    {
        //        stopwatch.Reset();
        //        sortalg.Pole = Generator.Generuj(i, max, min);
        //        stopwatch.Start();
        //        sortalg.Sort();
        //        stopwatch.Stop();
        //        serie.Points.Add(new DataPoint(i, stopwatch.Elapsed.TotalMilliseconds));
        //    }
            
        //    series.Add(sortalg.Name, serie);
        //}
    }
}
